# README #

This is a bash script to convert scanned handwritten notes into vectorized images.
A vectorized pdf is about twice as large as a 600 dpi pdf with scanned handwritten notes.

### What do I need to run the script? ###

* ImageMagick installed for "convert".
* potrace ([http://potrace.sourceforge.net/](http://potrace.sourceforge.net/)) either installed or the binary located in the folder together with the script.
* A pdf file containing 600 dpi monochrome images. However this might not be very strict. Other resolutions and non-monochrome images might work as well.