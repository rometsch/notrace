#!/bin/bash

show_help() {
cat << EOF
Usage info...
EOF
}

# Initialize our own variables:
input_file=""
output_file=""
blur_option="6x4"
threshold_option="45%"

while [ "$#" -gt 0 ]; do
	case $1 in
		-h|-\?|\?|--help)   # Call a "show_help" function to display a synopsis, then exit.
             show_help
             exit
             ;;
		-i|--input)       # Takes an option argument, ensuring it has been specified.
             if [ "$#" -gt 1 ]; then
				input_file="$2"
				shift 2
				continue
			else
				echo 'ERROR: Must specify a non-empty "--file FILE" argument.' >&2
				exit 1
			fi
			;;
		-o|--out)	
			output_file="$2"
			shift 2
			;;
		-b|--blur)
			blur_option="$2"
			shift 2
			;;
		-t|--threshold)
			threshold_option="$2"
			shift 2
			if [[ -z "$(echo "$2" | grep "%")" ]]; then
				echo "WARN: Threshold option should contain a %." >&2
			fi
			;;
		--)              # End of all options.
			shift
			break
			;;
		-?*)
			printf 'WARN: Unknown option (ignored): %s\n' "$1" >&2
			;;
		*)               # Default case: If no more options then break out of the loop.
			input_file="$1"	# Assume input file will be given without any option.
			break
		esac
done

# Suppose --file is a required option. Check that it has been set.
if [ ! "$input_file" ]; then
	echo 'ERROR: option "--file FILE" not given. See --help.' >&2
	exit 1
fi

# Check if convert is available.
command -v convert >/dev/null 2>&1 || { echo "ERROR: convert is required but it's not installed.  Aborting."  >&2; exit 1; }
# Check if potrace is available.
if [ -n "$(command -v potrace)" ]
then
	potrace_cmd=potrace
elif [ -f "./potrace" ]
then
	potrace_cmd=./potrace
else 
	echo "ERROR: potrace is required but it's not installed and not in the folder.  Aborting." >&2; exit 1;
fi

# Check specified input file.
if [[ -z "$(echo "$1" | grep .pdf)" ]]
then
	echo "ERROR: $input_file is not a pdf file.  Aborting." >&2; exit 1;
else
	infile="$1"
fi
# Check if output file is specified and generate a filename otherwise. 
if [[ -z "$output_file" ]]
then
	output_file="$(echo $input_file | sed 's/.pdf/-traced.pdf/')"
fi

# Convert the pdf into multiple png.
convert -density 600 $infile -blur "$blur_option" -threshold "$threshold_option" pbm:- | "$potrace_cmd" -b pdfpage -M 0 > $output_file